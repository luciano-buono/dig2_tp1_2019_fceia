/*
 * Copyright 2016-2018 NXP
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * o Redistributions of source code must retain the above copyright notice, this list
 *   of conditions and the following disclaimer.
 *
 * o Redistributions in binary form must reproduce the above copyright notice, this
 *   list of conditions and the following disclaimer in the documentation and/or
 *   other materials provided with the distribution.
 *
 * o Neither the name of NXP Semiconductor, Inc. nor the names of its
 *   contributors may be used to endorse or promote products derived from this
 *   software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
 
/**
 * @file    Dig2_TP1_2019.c
 * @brief   Application entry point.
 */
/*
 * Trabajo Practico 1, Sistemas Digitales 2, FCEIA UNR, 2019
 * Integrantes: Buono Luciano y Guillermo Ascolani
 */

#include <stdio.h>
#include <stdbool.h>
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"
#include "clock_config.h"
#include "MKL46Z4.h"
#include "fsl_debug_console.h"
#include "SD2_board.h"

/* TODO: insert other include files here. */

/* TODO: insert other definitions and declarations here. */
typedef enum{
	EST_MODO_PRINCIPAL, EST_MODO_FREQ, EST_MODO_INTENSITY,
}estMefGlobal_enum;

typedef enum{
	EST_LED_ROJO, EST_LED_VERDE, EST_LED_TODOS,
}estMefLed_enum;

typedef enum{
	EST_FREQ_1, EST_FREQ_2, EST_FREQ_3,EST_FREQ_4,
}estMefFreq_enum;

typedef enum{
	EST_INTENSITY_1,EST_INTENSITY_2,EST_INTENSITY_3,EST_INTENSITY_4,
}estMefIntensity_enum;



uint8_t MODO_LED=0;
uint8_t MODO_FREQ=0;
estMefLed_enum estMefLed;
uint16_t isLedRojoOn, isLedVerdeOn;
uint16_t SysTick_temp_Led = 0;
uint16_t SysTick_temp_blink = 0;
#define TIEMPO_LEDS1  1000
#define TIEMPO_LEDS3  3000


void mef_global();
void Mef_Principal();
uint8_t mef_Freq();
uint8_t mef_Intensity();
int main(void) {


	// Se inicializan funciones de la placa, Leds y TPM
	board_init();
	// Inicializa keyboard
	key_init();
	/* inicializa interrupción de systick cada 1 ms */
	SysTick_Config(SystemCoreClock / 1000U);


	SysTick_temp_Led = TIEMPO_LEDS1;
	SysTick_temp_blink = 500;
    PRINTF("Hello World\n");

    for(;;){

    	mef_global();
    }

    return 0 ;
}



void mef_global(){
	static estMefGlobal_enum estMefGlobal;
	switch(estMefGlobal){
	case EST_MODO_PRINCIPAL:
		Mef_Principal();
		MODO_LED=1;
		if (key_getPressEv(BOARD_SW_ID_1) ){
			estMefGlobal = EST_MODO_FREQ;
			board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_OFF);
			board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_OFF);
			MODO_LED=0;
		}
		if (key_getPressEv(BOARD_SW_ID_3) ){
			estMefGlobal = EST_MODO_INTENSITY;
			//estMefIntensity = EST_INTENSITY_1;
			MODO_LED=0;
		}
		break;
	case EST_MODO_FREQ:

		MODO_FREQ=1;
		if (mef_Freq() ){
			estMefGlobal = EST_MODO_PRINCIPAL;
			MODO_FREQ=0;
		}
		break;

	case EST_MODO_INTENSITY:
		if (mef_Intensity() ){
			estMefGlobal = EST_MODO_PRINCIPAL;
			board_setLedBrightness(BOARD_LED_ID_ROJO, 100);
			board_setLedBrightness(BOARD_LED_ID_VERDE, 100);
		}

		break;
	default: EST_MODO_PRINCIPAL;
	}
}

void Mef_Principal(){

	switch(estMefLed){
	case EST_LED_ROJO:

		board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_ON);
		board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_OFF);
		isLedRojoOn = 1;
		isLedVerdeOn = 0;
		if (SysTick_temp_Led ==0){
			SysTick_temp_Led = TIEMPO_LEDS1;
			estMefLed = EST_LED_VERDE;
		}
		break;

	case EST_LED_VERDE:

		board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_OFF);
		board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_ON);
		isLedRojoOn = 0;
		isLedVerdeOn = 1;
		if (SysTick_temp_Led ==0){
			SysTick_temp_Led = TIEMPO_LEDS3;
			estMefLed = EST_LED_TODOS;

		}
		break;
	case EST_LED_TODOS:
		board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_ON);
		board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_ON);
		isLedRojoOn = 1;
		isLedVerdeOn = 1;
		if (SysTick_temp_Led ==0){
			SysTick_temp_Led = TIEMPO_LEDS1;
			estMefLed = EST_LED_ROJO;
		}
		break;
	default: EST_LED_ROJO;
	}
}
uint8_t mef_Freq(void){
	static estMefFreq_enum estMefFreq=EST_FREQ_1;
	uint8_t isSW3Pressed = 0;
	switch(estMefFreq){
	case EST_FREQ_1:
		if(SysTick_temp_blink ==0){
			if (isLedRojoOn)
				board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_TOGGLE);
			if(isLedVerdeOn)
				board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_TOGGLE);
			SysTick_temp_blink=500;
			}

		if (key_getPressEv(BOARD_SW_ID_1) )
			estMefFreq= EST_FREQ_2;
		break;
	case EST_FREQ_2:
		if(SysTick_temp_blink ==0){
			if (isLedRojoOn)
				board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_TOGGLE);
			if(isLedVerdeOn)
				board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_TOGGLE);
			SysTick_temp_blink=350;

		}
		if (key_getPressEv(BOARD_SW_ID_1) )
			estMefFreq= EST_FREQ_3;
		break;

	case EST_FREQ_3:
		if(SysTick_temp_blink ==0){
			if (isLedRojoOn)
				board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_TOGGLE);
			if(isLedVerdeOn)
				board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_TOGGLE);
			SysTick_temp_blink=250;

		}
		if (key_getPressEv(BOARD_SW_ID_1) )
			estMefFreq= EST_FREQ_4;
		break;

	case EST_FREQ_4:
		if(SysTick_temp_blink ==0){
			if (isLedRojoOn)
				board_setLed(BOARD_LED_ID_ROJO, BOARD_LED_MSG_TOGGLE);
			if(isLedVerdeOn)
				board_setLed(BOARD_LED_ID_VERDE, BOARD_LED_MSG_TOGGLE);
			SysTick_temp_blink=100;

		}
		if (key_getPressEv(BOARD_SW_ID_1) )
			estMefFreq= EST_FREQ_1;
		break;

	default: EST_FREQ_1;
	}
	if (key_getPressEv(BOARD_SW_ID_3) ){
		estMefFreq = EST_FREQ_1;
		return isSW3Pressed=1;
	}
	return isSW3Pressed;
}
uint8_t mef_Intensity(void){
	static estMefIntensity_enum estMefIntensity=EST_INTENSITY_1;
	uint16_t dutycycle;
	uint8_t isSW1Pressed =0;
	switch(estMefIntensity){
	case EST_INTENSITY_1:

		if(isLedRojoOn){
			dutycycle = 100;
			board_setLedBrightness(BOARD_LED_ID_ROJO, dutycycle);
			}
		if (isLedVerdeOn){
			dutycycle = 100;
			board_setLedBrightness(BOARD_LED_ID_VERDE, dutycycle);
		}


		if (key_getPressEv(BOARD_SW_ID_3) )
			estMefIntensity= EST_INTENSITY_2;
		break;

	case EST_INTENSITY_2:
		if(isLedRojoOn){
			dutycycle = 65;
			board_setLedBrightness(BOARD_LED_ID_ROJO, dutycycle);
			}
		if (isLedVerdeOn){
			dutycycle = 65;
			board_setLedBrightness(BOARD_LED_ID_VERDE, dutycycle);
		}


		if (key_getPressEv(BOARD_SW_ID_3) )
			estMefIntensity= EST_INTENSITY_3;
		break;

	case EST_INTENSITY_3:
		if(isLedRojoOn){
			dutycycle = 20;
			board_setLedBrightness(BOARD_LED_ID_ROJO, dutycycle);
			}
		if (isLedVerdeOn){
			dutycycle = 20;
			board_setLedBrightness(BOARD_LED_ID_VERDE, dutycycle);
		}


		if (key_getPressEv(BOARD_SW_ID_3) )
			estMefIntensity= EST_INTENSITY_4;
		break;
	case EST_INTENSITY_4:
		if(isLedRojoOn){
			dutycycle = 8;
			board_setLedBrightness(BOARD_LED_ID_ROJO, dutycycle);
			}
		if (isLedVerdeOn){
			dutycycle = 8;
			board_setLedBrightness(BOARD_LED_ID_VERDE, dutycycle);
		}


		if (key_getPressEv(BOARD_SW_ID_3) )
			estMefIntensity= EST_INTENSITY_1;
		break;

	default: EST_INTENSITY_1;
	}
	if (key_getPressEv(BOARD_SW_ID_1) ){
		estMefIntensity = EST_INTENSITY_1;
		return isSW1Pressed = 1;}
	return isSW1Pressed;
}

void SysTick_Handler(void){
	key_periodicTask1ms();
	if (MODO_LED==1 ){
		if (SysTick_temp_Led > 0)
			SysTick_temp_Led --;
	}
	if (MODO_FREQ==1){
		if (SysTick_temp_blink > 0)
			SysTick_temp_blink --;
	}
}
